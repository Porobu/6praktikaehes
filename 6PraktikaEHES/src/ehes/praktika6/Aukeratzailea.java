package ehes.praktika6;

public class Aukeratzailea {
	public void aukeratu(Nagusia nireNagusia) {
		String[] args = nireNagusia.getArgumentuak();
		for (int i = 0; i < args.length; i++) {
			if (args[i].toLowerCase().contains("-train")) {
				nireNagusia.setTrainPath(args[i + 1]);
			} else if (args[i].toLowerCase().contains("-test")) {
				nireNagusia.setTestPath(args[i + 1]);
			} else if (args[i].toLowerCase().contains("-e")) {
				nireNagusia.setEzEkortu(true);
				System.out.println("RandomForest Sailkatzaileraren parametroak ez dira ekortuko");
			}
		}
		if (nireNagusia.getTestPath() == null || nireNagusia.getTrainPath() == null) {
			System.err.println("Train edo test fitxategiaren path-a ez da lortu!");
			System.exit(1);
		}
	}

}
