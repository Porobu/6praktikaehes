package ehes.praktika6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import weka.core.Instances;

public class InstantziaOperazioak {

	public Instances instantziakKargatu(String path) throws FileNotFoundException, IOException {
		System.out.println("Instantziak " + path + " kargatuko dira.");
		return new Instances(new BufferedReader(new FileReader(new File(path))));
	}
}
