package ehes.praktika6.inferentzia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.CVParameterSelection;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.Utils;

public class RandomForestEkortu {

	private void informazioaGorde(Evaluation ebaluatzailea, Classifier sailkatzailea, String path) throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path)));
		bw.write(sailkatzailea.getClass().getSimpleName() + " sailkatzaileraren irteera estandarra");
		bw.write(ebaluatzailea.toSummaryString());
		bw.write(ebaluatzailea.toClassDetailsString());
		bw.write(ebaluatzailea.toMatrixString());
		bw.flush();
		bw.close();
	}

	private void inprimatuWeka(Evaluation ebaluatzailea) throws Exception {
		System.out.println("\n=== Summary ===");
		System.out.println(ebaluatzailea.toSummaryString());
		System.out.println(ebaluatzailea.toClassDetailsString());
		System.out.println(ebaluatzailea.toMatrixString());
	}

	private String laburpenarenPathLortu(String pathZaharra, Classifier sailkatzailea, String mota) {
		for (int i = pathZaharra.length() - 1; i > 0; --i) {
			if (pathZaharra.charAt(i) == '/' || pathZaharra.charAt(i) == '\\') {
				return pathZaharra.substring(0, i) + "/" + sailkatzailea.getClass().getSimpleName() + "." + mota
						+ ".txt";
			}
		}
		return new String();
	}

	private void parametroakEkortu(Instances test, Instances train, String path) throws Exception {
		System.out.println("\nRandomForest sailkatzailearen parametro egokienak bilatzen...");
		CVParameterSelection cv = new CVParameterSelection();
		RandomForest rf = new RandomForest();
		cv.addCVParameter("M 0 1000 5");
		cv.setNumFolds(2);
		rf.setNumExecutionSlots(0);
		cv.setClassifier(rf);
		Instances osoa = new Instances(test);
		osoa.addAll(train);
		cv.buildClassifier(osoa);
		String[] hoberenak = cv.getBestClassifierOptions();
		String aukerak = Utils.joinOptions(hoberenak);
		System.out.println("Parametro egokienak: " + aukerak);
		this.rfEgin(test, train, path, hoberenak);
	}

	public void rf(Instances test, Instances train, String path, boolean ezEkortu) throws Exception {
		if (ezEkortu)
			this.trainVStest(test, train, new RandomForest(), path);
		else
			this.parametroakEkortu(test, train, path);
	}

	private void rfEgin(Instances test, Instances train, String path, String[] aukeraHoberenak) throws Exception {
		System.out.println("RandomForest aplikatu:");
		RandomForest rf = new RandomForest();
		rf.setOptions(aukeraHoberenak);
		rf.setNumExecutionSlots(0);
		this.trainVStest(test, train, rf, path);
	}

	private void trainVStest(Instances test, Instances train, Classifier sailkatzailea, String pathZaharra)
			throws Exception {
		System.out.println("\n" + sailkatzailea.getClass().getSimpleName() + " sailkatzailearekin train vs test:");
		Evaluation ebaluatzailea = new Evaluation(test);
		sailkatzailea.buildClassifier(train);
		ebaluatzailea.evaluateModel(sailkatzailea, test);
		this.inprimatuWeka(ebaluatzailea);
		this.informazioaGorde(ebaluatzailea, sailkatzailea,
				this.laburpenarenPathLortu(pathZaharra, sailkatzailea, "trainVStest"));
	}

}
