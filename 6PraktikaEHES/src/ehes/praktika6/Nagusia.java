package ehes.praktika6;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.lang3.time.StopWatch;

import ehes.praktika6.inferentzia.RandomForestEkortu;
import weka.core.Instances;

public class Nagusia {
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.err.println(
					"Programak funtzaionatzeko train, test fitxategien path-ak behar ditu gutxienez, edozein ordenetan.");
			System.err.println("Fitxategiak ematerakoan -test eta -train parametroak erabili behar dira, adibidez:");
			System.err.println("-test /path/to/test.arff -train /path/to/train.arff");
			System.err.println("-e parametroarekin RandomForest sailkatzaileraren parametroak ez dira ekortuko");
			System.exit(1);
		}
		Nagusia n = new Nagusia(args);
		n.hasieratu();
	}

	private Instances train, test;
	private String[] argumentuak;
	private InstantziaOperazioak io;
	private String testPath, trainPath;
	private Aukeratzailea ak;
	private boolean ezEkortu;
	private RandomForestEkortu rfe;
	private StopWatch s;

	public Nagusia(String[] args) {
		this.argumentuak = args;
		io = new InstantziaOperazioak();
		ezEkortu = false;
		ak = new Aukeratzailea();
		rfe = new RandomForestEkortu();
		s = new StopWatch();
	}

	public String[] getArgumentuak() {
		return argumentuak;
	}

	public String getTestPath() {
		return testPath;
	}

	public String getTrainPath() {
		return trainPath;
	}

	private void hasieratu() throws Exception {
		s.start();
		ak.aukeratu(this);
		this.instantziakKargatu();
		this.laburpena();
		this.klaseaIpini();
		rfe.rf(test, train, testPath, ezEkortu);
		s.stop();
		System.out.println("Erabilitako denbora: " + s.getTime() / 1000D + "s");
	}

	private void instantziakKargatu() throws FileNotFoundException, IOException {
		this.test = io.instantziakKargatu(testPath);
		this.train = io.instantziakKargatu(trainPath);
	}

	private void klaseaIpini() {
		test.setClassIndex(test.numAttributes() - 1);
		train.setClassIndex(train.numAttributes() - 1);
	}

	private void laburpena() {
		System.out.println("\nInstantzien laburpena:");
		System.out.println("Train multzoa:");
		System.out.println("Atributu Kopurua: " + train.numAttributes());
		System.out.println("Instantzia Kopurua: " + train.numInstances());
		System.out.println("Test multzoa:");
		System.out.println("Atributu Kopurua: " + test.numAttributes());
		System.out.println("Instantzia Kopurua: " + test.numInstances());
	}

	public void setEzEkortu(boolean ezEkortu) {
		this.ezEkortu = ezEkortu;
	}

	public void setTestPath(String testPath) {
		this.testPath = testPath;
	}

	public void setTrainPath(String trainPath) {
		this.trainPath = trainPath;
	}
}
